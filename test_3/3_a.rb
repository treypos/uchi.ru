# 3) Найдите вхождения каждого элемента в массив
# [ nil, 2, :foo, “bar”, “foo”, “apple”, “orange”, :orange, 45, nil,
# :foo, :bar, 25, 45, :apple, “bar”, nil]
# чтобы на выходе получился Hash по типу { элемент => количество вхождений в
# массив}

arr = [ nil, 2, :foo, "bar", "foo", "apple", "orange", :orange, 
45, nil, :foo, :bar, 25, 45, :apple, 'bar', nil]

hash = arr.each_with_object(Hash.new(0)) { |word,counts| counts[word] += 1 }

p hash

# или

p arr.tally

# или

p arr.group_by{|i| i.itself}.transform_values{|i| i.count}