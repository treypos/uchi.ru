# 2) Есть массив
# arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]

# a) напишите выражение, которое получает массив всех ключей

arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]

keys = []

arr.each do |item| 
  item.each do |k, v| 
    keys << k
  end
end

p keys

# или

arr.each {|key| keys.concat(key.keys)}
p keys