# 2) Есть массив
# arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]

# с) напишите выражение, которое получает сумму всех значений

arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]

values = []

arr.each {|value| values.concat(value.values)}
p values.sum