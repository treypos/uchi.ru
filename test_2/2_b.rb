# 2) Есть массив
# arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]

# b) напишите выражение, которое получает массив всех значений

arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]

values = []

arr.each do |item| 
  item.each do |_k, v| 
    values << v
  end
end

p values

# или

arr.each {|value| values.concat(value.values)}
p values