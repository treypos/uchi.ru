# Напишите функцию, которая имитирует работу светафора

# sa) на вход она получает один из цветов в виде строки (‘red’, ‘green’, ‘yellow’ ), на выходе
# будет результат (идти, стоять или ждать)

def traffic_light(color) 
  {'red' => 'Stop', 'yellow' => 'Wait', 'green' => 'Go'}[color]
end

p traffic_light('red')
p traffic_light('yellow')
p traffic_light('green')