# 4) Напишите функцию

# с) необязательно, но будет плюсом Напишите обработку ошибок, если юзер ввел
# неправильные данные (программа должна просить ввести число заново и сообщать об
# ошибке, но не прерываться)

def is_a_number?(cels)
  !!(Float(cels) rescue false)
end

def cels_to_fahr(cels)
cels.to_i * 1.8 + 32
end

loop do 
  print 'Введите температуру в градусах цельсия: '
  cels = gets
  if is_a_number?(cels) 
      puts cels_to_fahr(cels)
      exit
  else 
      puts 'Нужно ввести число!'
  end
end
