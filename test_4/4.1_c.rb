# Напишите функцию, которая имитирует работу светафора

# c) необязательно, но будет плюсом напишите обработку некорректных данных и
# добавьте возможность юзеру завершить работу программы

def traffic_light(color) 
  {'red' => 'Stop', 'yellow' => 'Wait', 'green' => 'Go'}[color]
end

loop do

  print 'Введите название цвета(red, yellow, green, stop - чтобы выйти): '

  color = gets.strip.downcase

  if color == 'red'
    puts traffic_light(color)
  elsif color == 'yellow'
    puts traffic_light(color)
  elsif color == 'green'
    puts traffic_light(color)
  elsif color == 'stop' 
    puts 'Программа завершается...'
    break
  else 
    puts 'Простите, я вас не понимаю :( '
  end

end
