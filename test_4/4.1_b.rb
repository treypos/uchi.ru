# Напишите функцию, которая имитирует работу светафора

# b) напишите это в виде консольной программы, которая не прекращает работу после
# однократного вызова, а ждет следующих запросов

def traffic_light(color) 
  {'red' => 'Stop', 'yellow' => 'Wait', 'green' => 'Go'}[color]
end

while true
  print 'Введите название цвета(red, yellow, green): '
  color = gets.chomp
  p traffic_light(color)
end
