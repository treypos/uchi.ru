-- 5) Обязательное задание.
-- Есть таблица students с колонками 
-- id int 
-- name varchar 
-- created_at datetime 
-- parent_id int 

-- a) посчитайте количество всех студентов 

select COUNT(*) from students;

-- b) посчитайте количество студентов с именем Иван

select COUNT(*) from students where name = 'Иван';
-- скорее всего в таблице имена студентов будут записаны в формате ФИО
 select COUNT(*) from book where title like '_% Иван _%'

-- c) посчитайте количество студентов созданных после 1 сентября 2020 года

select COUNT(*) from students where created_at >= '2020-09-02 00:00:00';
